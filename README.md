# QMCMC Exploration

## Name
Quantum-enhanced Markov Chain Monte Carlo using Warm-Start QAOA

## Description
This project aims to implement and explore the implications of [warm-starting QAOA](https://qiskit-community.github.io/qiskit-optimization/tutorials/10_warm_start_qaoa.html) within the context of the [quantum-enhanced MCMC](https://arxiv.org/abs/2203.12497) algorithm proposed for sampling from Boltzmann distributions of classical Ising models. The integration of warm-started QAOA into the MCMC iterations provides a potential solution to the exploration challenges posed by rugged landscapes at low temperatures. Through this project, we seek to assess the efficacy and performance of this approach, offering insights into its applicability.

## Usage
The tutorials include
- an [initial demo](https://gitlab.com/Amana.L/QMCMC/-/blob/main/tutorials/00_QMCMC_with_WarmStart_QAOA.ipynb) including an in-depth explanation of the methods being used.
- a [comparison](https://gitlab.com/Amana.L/QMCMC/-/blob/main/tutorials/01_Compare_with_HybridSolver.ipynb) with the results given by Quantagonia's HybridSolver.
- an [alteration](https://gitlab.com/Amana.L/QMCMC/-/blob/main/tutorials/02_altered_MH_with_dE_0.ipynb) to the metropolis-hastings
- [changing the parameters](https://gitlab.com/Amana.L/QMCMC/-/blob/main/tutorials/03_changes_to_QAOA_params.ipynb) of the QAOA to improve results
